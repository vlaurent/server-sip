# Sip server for Scenic

This project was created from the [scenic-server repository](https://gitlab.com/sat-metalab/scenic-server). Its goal is to decompose the server configuration in order to have a better understand of it.

## Dependencies

+ [Docker](https://www.docker.com/)
+ [Docker Compose](https://docs.docker.com/compose/)
+ [Alpine Linux](https://alpinelinux.org/)
+ [Freeswitch](https://freeswitch.org/confluence/display/FREESWITCH/Introduction)

## Run it
```bash
docker-compose up
```